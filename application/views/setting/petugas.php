<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Setting User
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
		 <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Petugas List</h3>
                    <div class="box-tools">
                       
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
				
				<form role="form" id="addUser" action="<?php echo base_url() ?>setting/storeNewUser" method="post" role="form">
                               <div class="box-body">
							   <div id="temp"></div>
                            <div class="s">
                                <div class="col-md-6" style="margin-left:-6px;">                          
                                    <div class="form-group">
                                        <label for="fname">From User</label>
                                        <select name="from" class="form-control" autofocus>
											<option value=""></option>
											<?php foreach($users as $f) { ?>
												<option value="<?php echo $f->id_usr;?>"><?php echo $f->id_usr;?> - <?php echo $f->nama;?></option>
											<?php } ?>
										</select>
                                    </div>
                                    
                                </div>
                               
                            </div>
							
								<div class="clearfix"></div>
							
							 <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Create New User" />
                           
                        </div>
                            
                            
							<div class="row">
								<div class="col-md-12">
				
                  <table class="table table-hover">
				<thead>
                    <tr>
						<th>NIP</th>
					  <th>Nama</th>
					<Th>Jabatan</th>
                    
                    </tr>
					</thead>
					<tbody>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
				
					<td><input value="<?php echo $record->nip;?>" class="checkboxs" data-col="<?php echo $record->nip;?>" type="checkbox" id="aksess_<?php echo $record->nip;?>"/> 
					<?php echo $record->nip; ?></td>
                      <td><?php echo $record->nama; ?></td>
					   <td><?php echo $record->nm_jbtn; ?></td>
                      
                    </tr>
                    <?php
                        }
                    }
                    ?>
					</tbody>
                  </table>
                  </div>
							</div>
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Create New User" />
                            
                        </div>
                    </form>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>

<script>
	$(document).ready(function()
	{
			$(".checkboxs").click(function()
			{
				var checked = $(this).is(":checked");
				var val = $(this).val();
				var col = $(this).data("col");
				
				if(checked)
				{
					
					$("#temp").append("<input type='hidden' class='" + col + "' name='akses_"  + col + "' value='" + val + "'/>");
				}
				else{
					$("#temp").find("." + col).remove();
				}
			});
	});
</script>